import { SidePanel } from './core/SidePanel';
import { Spinner } from './core/Spinner';
import { Spinner2 } from './core/Spinner2';
import DemoContextAndReducer from './demo/DemoContextAndReducer';
import ZustandCrud from './demo/ZustandCrud';
import ZustandDemo1 from './demo/ZustandDemo1';

export default function App() {
  return <div>
    {/*<DemoContextAndReducer />*/}
    {/* <ZustandDemo1 />*/}
    <ZustandCrud />

    <h1>Components</h1>
    <Spinner2 />
    <Spinner2 size="xl"/>
    <Spinner />
    <Spinner size="md" />
    <Spinner size="xl" />

   <SidePanel title="DETTAGLI">
     Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus alias aut cum dolorum error et exercitationem, fuga fugit incidunt iusto libero molestiae necessitatibus perferendis porro, sequi totam voluptate? Consequatur, totam?
     Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus alias aut cum dolorum error et exercitationem, fuga fugit incidunt iusto libero molestiae necessitatibus perferendis porro, sequi totam voluptate? Consequatur, totam?
     Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus alias aut cum dolorum error et exercitationem, fuga fugit incidunt iusto libero molestiae necessitatibus perferendis porro, sequi totam voluptate? Consequatur, totam?
   </SidePanel>
  </div>
}
