
export interface AppState {
  value1: number;
  value2: number
}

export interface AppActions {
  type: 'increment' | 'updateValue2';
  payload: number;
}

export function appReducer(state: AppState, action: AppActions) {
  switch (action.type) {
    case 'increment':
      return { ...state,  value1: state.value1 + action.payload}
    case 'updateValue2':
      return { ...state,  value2: state.value2 + action.payload}
    default:
      return state;
  }
}
