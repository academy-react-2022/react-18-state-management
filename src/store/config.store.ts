import create from 'zustand';

export type Theme = 'dark' | 'light';

type AppState = {
  theme: Theme;
  counter: number;
  sidePanelOpened: boolean;
  increment: () => void;
  changeTheme: (theme: Theme) => void;
  toggleSidePanel: () => void;
}
export const configStore = create<AppState>((set, get) => ({
  theme: 'dark',
  counter: 0,
  sidePanelOpened: false,
  increment: () => set(s => ({ counter: s.counter + 1 })),
  changeTheme: (th: Theme) => set(() => ({ theme: th })),
  toggleSidePanel: () => set(s => ({ sidePanelOpened: !s.sidePanelOpened}))
}))

