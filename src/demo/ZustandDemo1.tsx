import clsx from 'clsx';
import React from 'react';
import { configStore } from '../store/config.store';

export default function ZustandDemo1 () {
  return <div >
    <NavBar />
    <Page />
    <Panel />
  </div>
};


export function NavBar( ) {
  const theme = configStore(state => state.theme)
  const count =  configStore(state => state.counter)
  const toggle =  configStore(state => state.toggleSidePanel)

  return <div className={clsx({
    'bg-dark text-white': theme === 'dark',
    'bg-info': theme === 'light'
  })}>
    item | item | item {count}
    <button onClick={toggle}>Open Side Panel</button>
  </div>
}


export function Page( ) {
  const theme = configStore(state => state.theme)
  const inc = configStore(state => state.increment)
  const changeTheme = configStore(state => state.changeTheme)

  return <div>
    <button
      className={clsx({ 'bg-warning': theme === 'dark'})}
      onClick={() => changeTheme('dark')}>Dark</button>
    <button
      className={clsx({ 'bg-warning': theme === 'light'})}
      onClick={() => changeTheme('light')}>Light</button>
    <button onClick={inc}>+</button>
  </div>
}


const Panel = (() => {
  console.log('panel')
  return <div>
    Panel
  </div>
})
