import React, { createContext, Dispatch, useCallback, useContext, useReducer, useState } from 'react';

// ====== model/app.state.ts
interface AppState {
  value1: number;
  value2: string;
}

// ====== counter.actions.ts
interface Increment {
  type: 'increment',
  payload: number;
}
interface SetRandom {
  type: 'setRandom',
  payload: string;
}

type Actions = Increment | SetRandom;


// ===== abc.reducer.ts
function reducer(s: AppState, action: Actions) {
  switch (action.type) {
    case 'increment':
      return {...s, value1: s.value1 + action.payload}
    case 'setRandom':
      return {...s, value2: action.payload}
  }
  return s;
}
// ======


const DataContext = createContext<AppState | null>(null)
const DispatchContext = createContext<Dispatch<Actions>>(() => null)

const DemoContextAndReducer = () => {
  const [state, dispatch] = useReducer(reducer, { value1: 0, value2: 'ciao'})

  return <DataContext.Provider value={state}>
    <DispatchContext.Provider value={dispatch}>
      <h1>App</h1>
      <Panel1 />
      <Panel1B />
    </DispatchContext.Provider>
  </DataContext.Provider>
}


const Panel1 = React.memo(() => {
  const state = useContext(DataContext);
  return <div className="comp">
    Panel1: {state?.value1} - {state?.value2}
  </div>
})
const Panel1B = React.memo(() => {
  const dispatch = useContext(DispatchContext);
  return <div className="comp">
    Panel1B:
    {/*<button onClick={() => dispatch({ type: 'decrement', payload: 5})}>-</button>*/}
    <button onClick={() => dispatch({ type: 'increment', payload: 10})}>+</button>
    <button onClick={() => dispatch({ type: 'setRandom', payload: 'fabio'})}>set Name</button>
  </div>
})


export default DemoContextAndReducer;

