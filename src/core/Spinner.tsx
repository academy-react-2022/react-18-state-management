import clsx from 'clsx';
import css from './Spinner.module.css';

export interface SpinnerProps {
  size?: 'sm' | 'md' | 'xl';
}

const SIZES = {
  'sm': 30,
  'md': 50,
  'xl': 80,
}

export function Spinner(props: SpinnerProps) {
  const { size = 'sm'} = props;

  return(
    <div className={clsx(
      css.pippo,
      'd-flex justify-content-center')
    }>
      <img
        width={SIZES[size]}
        src="https://media.istockphoto.com/photos/red-fidget-spinner-picture-id683837422?k=20&m=683837422&s=612x612&w=0&h=_QEKlFyGYRfC2TJqEZqP3_wN_PRzyNP6NCxy-tUr65U="
      />
    </div>
)}
