import clsx from 'clsx';
import { PropsWithChildren } from 'react';
import { configStore } from '../store/config.store';
import css from './SidePanel.module.css'

interface SidePanelProps {
  title: string;
}
export function SidePanel(props: PropsWithChildren<SidePanelProps>) {
  const isOpened = configStore(state => state.sidePanelOpened)
  const toggle = configStore(state => state.toggleSidePanel)

  return <div
    className={clsx(
      css.sidepanel,
      { [css.open]: isOpened }
    )}
  >
    <h1>
      {props.title}
      <span onClick={toggle}>❌</span>
    </h1>
    <div style={{overflowY: 'scroll', height: '100vh'}}>
      {props.children}
    </div>
  </div>
}
